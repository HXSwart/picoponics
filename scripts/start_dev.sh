#!/bin/bash

IMAGE_NAME="dev_image"

# Check if the PROJ_ROOT environment variable is set
if [ -z "$PROJ_ROOT" ]; then
  echo "The PROJ_ROOT environment variable is not set."
  echo "Please ensure you have run 'source .envrc' in your project directory before running this script."
  exit 1
fi

# Check if the Docker image exists
if [[ "$(docker images -q $IMAGE_NAME 2> /dev/null)" == "" ]]; then
  echo "Docker image $IMAGE_NAME does not exist."
  echo "Please build the image before running this script."
  exit 1
fi

# Run the Docker container
docker run --privileged \
           -it \
           --rm \
           -v /dev/bus/usb:/dev/bus/usb \
           -v "$PROJ_ROOT/src:/usr/src/app" \
           -p 5000:5000 \
           $IMAGE_NAME

# Explanation of flags used:
# --privileged: Allows the container to access host devices.
# -it: Runs the container in interactive mode with a TTY, so you can interact with the process.
# --rm: Automatically removes the container when it exits.
# -v /dev/bus/usb:/dev/bus/usb: Mounts the USB device directory inside the container, enabling USB access.
# -v "$PROJ_ROOT:/usr/src/app": Mounts the project root directory to the working directory in the container.
# -p 5000:5000: Exposes port 5000 from the container to port 5000 on the host, useful if your development environment includes a web server.

