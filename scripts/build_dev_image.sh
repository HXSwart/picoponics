#!/bin/bash

IMAGE_NAME="dev_image"
IMAGE_TAG="latest"

echo "Building dev image image: $IMAGE_NAME:$IMAGE_TAG"

docker build -t $IMAGE_NAME:$IMAGE_TAG .

if [ $? -eq 0 ]; then
    echo "Docker image built successfully."
else
    echo "Error: Docker build failed." >&2
    exit 1
fi

