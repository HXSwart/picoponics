FROM python:3.11-slim

RUN apt-get update && apt-get install -y \
    git \
    usbutils \
    && rm -rf /var/lib/apt/lists/*

RUN git clone https://github.com/micropython/micropython.git /usr/src/micropython

RUN pip install mpremote flask

WORKDIR /usr/src/app

EXPOSE 5000

ENV NAME DEV_ENV

CMD ["bash"]

