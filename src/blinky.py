from machine import Pin
import utime

led = Pin('LED', Pin.OUT)
print("You've successfully setup the picoponics dev env")    
while True:
    led.toggle()
    utime.sleep(2)
